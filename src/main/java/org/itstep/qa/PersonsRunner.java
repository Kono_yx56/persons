package org.itstep.qa;

import org.kohsuke.randname.RandomNameGenerator;

import java.util.Calendar;
import java.util.Random;

public class PersonsRunner {
    public static void main(String[] args) {
    Persons personses[] = new Persons[15];
    RandomNameGenerator rndName = new RandomNameGenerator(0);

    Calendar cal = Calendar.getInstance();
    Random rndDate = new Random();
    for (int i = 0; i < personses.length; i++) {
        cal.set(rndDate.nextInt(40) + 1970, rndDate.nextInt(11), rndDate.nextInt(27 ) + 1);
        personses[i] = new Persons(rndName.next(),cal.getTime());
        personses[i].print();
    }

    System.out.println("====================================");
    System.out.println("Сотрудники с \"it\" в имени:");
    for (int i = 0; i < personses.length; i++) {
        if (personses[i].getName().contains("it")) {
            personses[i].print();
        }
    }
}
}

