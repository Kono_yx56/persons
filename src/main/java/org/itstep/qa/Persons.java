package org.itstep.qa;

import org.kohsuke.randname.RandomNameGenerator;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Persons {

        private String name;
        private Date birthday;

        public Persons(String name, Date birthday) {
            this.name = name;
            this.birthday = birthday;
        }

    public String getName() {
            return name;
        }

        public void print() {
            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
            System.out.println("Name: " + name + ", birthday: " + dateFormat.format(birthday));
        }
    }
